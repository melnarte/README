# Get the sources

```bash
git clone git@gitlab.com:promethe/libs.git .
git clone git@gitlab.com:promethe/hardware.git hardware
git clone git@gitlab.com:promethe/tools.git scripts
git clone git@gitlab.com:promethe/user.git prom_user
git clone git@gitlab.com:promethe/pandora.git pandora
git clone git@gitlab.com:promethe/themis.git themis
git clone git@gitlab.com:promethe/coeos.git coeos
git clone git@gitlab.com:promethe/core.git prom_kernel
mv graphics graphique
```


# Build promethe

```bash
./scripts/install_all_devel.sh
```